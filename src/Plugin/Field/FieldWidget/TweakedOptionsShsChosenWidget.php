<?php

namespace Drupal\shs_tweak\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\shs_chosen\Plugin\Field\FieldWidget\OptionsShsChosenWidget;

class TweakedOptionsShsChosenWidget extends OptionsShsChosenWidget {

  /**
   * Predefined empty options array.
   * @see \Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase::getOptions()
   */
  public $options = [];

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if ($form_state->getFormObject()->getFormId() == 'field_config_edit_form') {
      $this->options = NULL;
      $element = parent::formElement($items, $delta, $element, $form, $form_state);
    }
    else {
      $element = parent::formElement($items, $delta, $element, $form, $form_state);

      $element['#type'] = 'textfield';
      unset($element['#options']);

      if (!$element['#default_value']) {
        $element['#default_value'] = '_none';
      }

      $element['#attached']['library'][] = 'shs_tweak/main';
    }

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  protected function getSelectedOptions(FieldItemListInterface $items) {
    $selected_options = [];
    foreach ($items as $item) {
      $selected_options[] = $item->target_id;
    }
    return $selected_options;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if (isset($values[0]['target_id']) && strpos($values[0]['target_id'], ',') !== FALSE) {
      $exploded_values = explode(',', $values[0]['target_id']);
      $values = [];
      foreach ($exploded_values as $value) {
        $values[]['target_id'] = $value;
      }
    }

    return $values;
  }

}
