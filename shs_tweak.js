(function ($, Drupal) {

  'use strict';

  var shsAttachFunctionString = Drupal.behaviors.shs.attach.toString();
  shsAttachFunctionString = shsAttachFunctionString.replace('select.shs-enabled:not([disabled])', '.shs-enabled:not([disabled])');
  Drupal.behaviors.shsTweak = {};
  eval('Drupal.behaviors.shsTweak.attach = ' + shsAttachFunctionString);

}(jQuery, Drupal));
